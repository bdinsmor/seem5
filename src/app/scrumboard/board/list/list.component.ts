import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef } from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { FusePerfectScrollbarDirective } from "src/app/@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive";
import { FuseConfirmDialogComponent } from "src/app/@fuse/components/confirm-dialog/confirm-dialog.component";
import { ScrumboardService } from "../../scrumboard.service";
import { Card } from "../../card.model";
import { ScrumboardCardDialogComponent } from "../dialogs/card/card.component";
import { Board } from "../../board.model";

@Component({
  selector: "scrumboard-board-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class ScrumboardBoardListComponent implements OnInit, OnDestroy {
  dialogRef: any;

  @Input()
  list;
  @Input()
  board: Board;

  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _matDialog: MatDialog
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {}

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * On list name changed
   *
   * @param newListName
   */
  onListNameChanged(newListName): void {
    this.list.name = newListName;
  }

  /**
   * On card added
   *
   * @param newCardName
   */
  onCardAdd(newCardName): void {
    if (newCardName === "") {
      return;
    }
  }

  /**
   * Remove list
   *
   * @param listId
   */
  removeList(listId): void {}

  /**
   * Open card dialog
   *
   * @param cardId
   */
  openCardDialog(cardId): void {
    this.dialogRef = this._matDialog.open(ScrumboardCardDialogComponent, {
      panelClass: "scrumboard-card-dialog",
      data: {
        cardId: cardId,
        listId: this.list.id
      }
    });
    this.dialogRef.afterClosed().subscribe(response => {});
  }

  /**
   * On drop
   *
   * @param ev
   */
  onDrop(ev): void {}
}
