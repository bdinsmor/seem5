import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { fuseAnimations } from "../@fuse/animations";
import { ScrumboardService } from "./scrumboard.service";
import { Board } from "./board.model";
import { ScrumboardFakeDb } from "./data";
import { List } from "./list.model";
@Component({
  selector: "scrumboard",
  templateUrl: "./scrumboard.component.html",
  styleUrls: ["./scrumboard.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ScrumboardComponent implements OnInit, OnDestroy {
  boards: any[];
  board: Board;
  constructor() {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.boards = ScrumboardFakeDb.boards;
    this.board = this.boards[0];
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * New board
   */
  newBoard(): void {
    const newBoard = new Board({});
  }

  /**
   * On list add
   *
   * @param newListName
   */
  onListAdd(newListName): void {
    if (newListName === "") {
      return;
    }

    // this._scrumboardService.addList(new List({ name: newListName }));
  }

  /**
   * On board name changed
   *
   * @param newName
   */
  onBoardNameChanged(newName): void {}

  /**
   * On drop
   *
   * @param ev
   */
  onDrop(ev): void {
    //  this._scrumboardService.updateBoard();
  }
}
