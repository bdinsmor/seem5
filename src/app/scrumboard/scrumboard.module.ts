import { NgModule } from "@angular/core";

import { RouterModule, Routes } from "@angular/router";
import {
  MatButtonModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatRippleModule,
  MatSidenavModule,
  MatToolbarModule,
  MatTooltipModule
} from "@angular/material";
import { NgxDnDModule } from "@swimlane/ngx-dnd";
import { ScrumboardComponent } from "./scrumboard.component";
import { ScrumboardService } from "./scrumboard.service";
import { ScrumboardBoardComponent } from "./board/board.component";
import { ScrumboardBoardListComponent } from "./board/list/list.component";
import { ScrumboardBoardCardComponent } from "./board/list/card/card.component";
import { ScrumboardBoardEditListNameComponent } from "./board/list/edit-list-name/edit-list-name.component";
import { ScrumboardBoardAddCardComponent } from "./board/list/add-card/add-card.component";
import { ScrumboardBoardAddListComponent } from "./board/add-list/add-list.component";
import { ScrumboardCardDialogComponent } from "./board/dialogs/card/card.component";
import { ScrumboardLabelSelectorComponent } from "./board/dialogs/card/label-selector/label-selector.component";
import { ScrumboardEditBoardNameComponent } from "./board/edit-board-name/edit-board-name.component";
import { ScrumboardBoardSettingsSidenavComponent } from "./board/sidenavs/settings/settings.component";
import { ScrumboardBoardColorSelectorComponent } from "./board/sidenavs/settings/board-color-selector/board-color-selector.component";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { FuseDirectivesModule } from "../@fuse/directives/directives";
import { FusePipesModule } from "../@fuse/pipes/pipes.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FuseMaterialColorPickerModule } from "../@fuse/components";
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  declarations: [
    ScrumboardComponent,
    ScrumboardBoardComponent,
    ScrumboardBoardListComponent,
    ScrumboardBoardCardComponent,
    ScrumboardBoardEditListNameComponent,
    ScrumboardBoardAddCardComponent,
    ScrumboardBoardAddListComponent,
    ScrumboardCardDialogComponent,
    ScrumboardLabelSelectorComponent,

    ScrumboardEditBoardNameComponent,
    ScrumboardBoardSettingsSidenavComponent,
    ScrumboardBoardColorSelectorComponent
  ],
  imports: [
    // Fuse modules
    CommonModule,
    BrowserModule,
    FormsModule,
    FlexLayoutModule,
    FuseMaterialColorPickerModule,
    ReactiveFormsModule,
    FuseDirectivesModule,
    FusePipesModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule
  ],

  entryComponents: [ScrumboardCardDialogComponent]
})
export class ScrumboardModule {}
